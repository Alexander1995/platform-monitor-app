#ifndef UART_H
#define UART_H
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>

class Uart : public QSerialPort
{
public:
    Uart();
    void setUartSettingsAndRun();
};

#endif // UART_H
