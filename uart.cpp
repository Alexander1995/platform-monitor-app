#include "uart.h"

Uart::Uart()
{    
    this->setUartSettingsAndRun();
}
void Uart::setUartSettingsAndRun()
{
    //this->setPort();
    this->open(QSerialPort::WriteOnly);
    this->setBaudRate(QSerialPort::Baud2400);
    this->setDataBits(QSerialPort::Data8);
    this->setParity(QSerialPort::NoParity);
    this->setStopBits(QSerialPort::OneStop);
    this->setFlowControl(QSerialPort::NoFlowControl);
}

