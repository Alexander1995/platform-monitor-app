#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "uart.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    upper->addButton(ui->radioButton);
    upper->addButton(ui->radioButton_2);
    upper->addButton(ui->radioButton_3);

    lower->addButton(ui->radioButton_4);
    lower->addButton(ui->radioButton_5);
    lower->addButton(ui->radioButton_6);

    grid->addWidget(ui->label, 0, 0);
    grid->addWidget(ui->radioButton, 1, 0);
    grid->addWidget(ui->radioButton_2, 2, 0);
    grid->addWidget(ui->radioButton_3, 3, 0);
    grid->addWidget(ui->textBrowser, 4, 0);

    grid->addWidget(ui->label_2, 0, 2);
    grid->addWidget(ui->radioButton_4, 1, 2);
    grid->addWidget(ui->radioButton_5, 2, 2);
    grid->addWidget(ui->radioButton_6, 3, 2);
    grid->addWidget(ui->textBrowser_2, 4, 2);

    ui->radioButton_2->setChecked(true);
    ui->radioButton_5->setChecked(true);

    ui->centralWidget->setLayout(grid);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_radioButton_clicked()
{
    uart.putChar(0x1A);
    ui->textBrowser->setText("Турель движется по часовой стрелке");
}

void MainWindow::on_radioButton_2_clicked()
{
    uart.putChar(0x1B);
    ui->textBrowser->setText("Останов");
}

void MainWindow::on_radioButton_3_clicked()
{
    uart.putChar(0x1C);
    ui->textBrowser->setText("Турель движется против часовой стрелки");
}

void MainWindow::on_radioButton_4_clicked()
{
    uart.putChar(0x2A);
    ui->textBrowser_2->setText("Турель движется по часовой стрелке");
}

void MainWindow::on_radioButton_5_clicked()
{
    uart.putChar(0x2B);
    ui->textBrowser_2->setText("Останов");
}

void MainWindow::on_radioButton_6_clicked()
{
    uart.putChar(0x2C);
    ui->textBrowser_2->setText("Турель движется против часовой стрелки");
}


