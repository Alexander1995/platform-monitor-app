#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGroupBox>
#include <QVBoxLayout>
#include <QButtonGroup>
#include "uart.h"

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_radioButton_clicked();
    void on_radioButton_2_clicked();
    void on_radioButton_3_clicked();
    void on_radioButton_4_clicked();
    void on_radioButton_5_clicked();
    void on_radioButton_6_clicked();

private:
    Ui::MainWindow *ui;
    Uart uart;
    QGroupBox box;
    QButtonGroup *upper = new QButtonGroup( &box );
    QButtonGroup *lower = new QButtonGroup( &box );
    QGridLayout *grid = new QGridLayout( &box );
};

#endif // MAINWINDOW_H
